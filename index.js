const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://150.136.144.249:27017/?readPreference=primary&ssl=false';
const dbName = 'maple';

const client = new MongoClient(url);
client.connect((err) => {
	if (err) {
		console.log(err.message);
	} else {
		try {
			const envName = 'aic-oracle';
			const db = client.db(dbName);
			const collection = db.collection('EnvConfig');
			collection.find({ envName: envName }).limit(1).toArray((err, docs) => {
				console.log(docs);
				client.close();
			});
		} catch (error) {
			console.log(error.message);
		}
	}
});
